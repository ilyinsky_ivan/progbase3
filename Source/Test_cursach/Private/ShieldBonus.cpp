// Fill out your copyright notice in the Description page of Project Settings.

#include "ShieldBonus.h"
#include "Test_cursachPawn.h"

void UShieldBonus::GiveEffect(ATest_cursachPawn * Pawn)
{
	Pawn->AddShield(existTime, shieldModifier);
	Pawn->OnAddBonusEffect.Broadcast(this);
}

float UShieldBonus::GetTime()
{
	return existTime;
}

FString UShieldBonus::SetString()
{
	return FString("Defence bonus");
}




