// Fill out your copyright notice in the Description page of Project Settings.

#include "FuelEconomyBonusComponent.h"
#include "Test_cursachPawn.h"

void UFuelEconomyBonusComponent::GiveEffect(ATest_cursachPawn * Pawn)
{
	Pawn->AddFuelEconomy(fuelEcExistTime, fuelEcModifier);
	Pawn->OnAddBonusEffect.Broadcast(this);
}

float UFuelEconomyBonusComponent::GetTime()
{
	return fuelEcExistTime;
}

FString UFuelEconomyBonusComponent::SetString()
{
	return FString("Fuel Economy Bonus");
}



