// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusComponent.h"
#include "FuelEconomyBonusComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TEST_CURSACH_API UFuelEconomyBonusComponent : public UBonusComponent
{
	GENERATED_BODY()

	virtual void GiveEffect(ATest_cursachPawn * Pawn) override;

	UFUNCTION(BlueprintCallable, Category = "Cursach")
	virtual float GetTime() override;

	UFUNCTION(BlueprintCallable, Category = "Cursach")
	virtual FString SetString() override;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursach")
	float fuelEcExistTime = 20.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursach")
	float fuelEcModifier = 100.0f;
	
};
