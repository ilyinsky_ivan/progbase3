// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusComponent.h"
#include "ShieldBonus.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TEST_CURSACH_API UShieldBonus : public UBonusComponent
{
	GENERATED_BODY()

	virtual void GiveEffect(ATest_cursachPawn * Pawn) override;

	UFUNCTION(BlueprintCallable, Category = "Cursach")
	virtual float GetTime() override;

	UFUNCTION(BlueprintCallable, Category = "Cursach")
	virtual FString SetString() override;
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursach")
	float existTime = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursach")
	float shieldModifier = 25.0f;
};
