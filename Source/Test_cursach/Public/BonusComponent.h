// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BonusComponent.generated.h"

class ATest_cursachPawn;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TEST_CURSACH_API UBonusComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBonusComponent();

//protected:
	// Called when the game starts
	//virtual void BeginPlay() override;

public:	
	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Coursach|Bonus")
	virtual void GiveEffect(ATest_cursachPawn * Pawn) {};

	UFUNCTION(BlueprintCallable, Category = "Coursach|Bonus")
	virtual float GetTime();

	UFUNCTION(BlueprintCallable, Category = "Coursach|Bonus")
	virtual FString SetString();
	
};
